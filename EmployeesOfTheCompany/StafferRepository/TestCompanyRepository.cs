﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository
{
    public class TestCompanyRepository : IStafferRepository
    {
        public IEnumerable<StafferLoadObject> GetAll()
        {
            var listOfStafferLoadObjects = new List<StafferLoadObject>()
            {
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 1,'Id': 5,'Name':'B','BeginWorkAt':'2012-10-12T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 6,'Name':'C','BeginWorkAt':'2014-11-10T00:00:00','IndexesOfSubordinated':[9,10,12]}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 2,'Id': 7,'Name':'D','BeginWorkAt':'2020-08-02T00:00:00','IndexesOfSubordinated':[21]}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 2,'Id': 3,'Name':'A','BeginWorkAt':'2010-12-01T00:00:00','IndexesOfSubordinated':[5,6,7]}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 1,'Id': 21,'Name':'H','BeginWorkAt':'2017-08-07T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 9,'Name':'E','BeginWorkAt':'2019-03-01T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 10,'Name':'F','BeginWorkAt':'2021-10-02T00:00:00','IndexesOfSubordinated':[14,15,18]}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 2,'Id': 12,'Name':'G','BeginWorkAt':'2018-04-05T00:00:00','IndexesOfSubordinated':[24,25]}"),

            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 14,'Name':'I','BeginWorkAt':'2017-08-07T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 1,'Id': 18,'Name':'K','BeginWorkAt':'2021-09-04T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 15,'Name':'J','BeginWorkAt':'2019-04-03T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 24,'Name':'L','BeginWorkAt':'2016-11-12T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 2,'Id': 25,'Name':'M','BeginWorkAt':'2021-10-29T00:00:00','IndexesOfSubordinated':null}"),

            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 31,'Name':'Q','BeginWorkAt':'2018-01-01T00:00:00','IndexesOfSubordinated':null}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 2,'Id': 30,'Name':'P','BeginWorkAt':'2018-01-01T00:00:00','IndexesOfSubordinated':[31,33]}"),
            JsonConvert.DeserializeObject<StafferLoadObject>(@"{'Category': 3,'Id': 33,'Name':'R','BeginWorkAt':'2018-01-01T00:00:00','IndexesOfSubordinated':null}")
            };

            // TO DO Checks: (for the uniqueness of the Id, not Empty etc.)

            return listOfStafferLoadObjects;
        }
    }
}

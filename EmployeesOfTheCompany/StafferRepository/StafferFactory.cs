﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.Entities;
using System;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository
{
    public static class StafferFactory
    {
        public static BaseForStaffer CreateStaffer(StafferLoadObject stafferLoadObject, int level)
        {
            return stafferLoadObject.Category switch
            {
                StufferCategories.Employee => new Employee(stafferLoadObject, level),
                StufferCategories.Manager => new Manager(stafferLoadObject, level),
                StufferCategories.Saler => new Saler(stafferLoadObject, level),
                _ => throw new ArgumentException($"stafferLoadObject.Category in {nameof(CreateStaffer)}"),
            };
        }
    }
}

﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository
{
    public class StafferLoadObject
    {
        public StufferCategories Category { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BeginWorkAt { get; set; }

        public List<int> IndexesOfSubordinated { get; set; } // To simplify it, I made it common (for Employee , it will be null)
        public StafferLoadObject()
        {
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

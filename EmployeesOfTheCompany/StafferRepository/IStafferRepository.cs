﻿using System.Collections.Generic;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository
{
    public interface IStafferRepository
    {
        IEnumerable<StafferLoadObject> GetAll();
    }
}

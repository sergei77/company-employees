﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.Entities;
using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany
{
    public class CompanyStaffers
    {
        private readonly IStafferRepository _stafferRepository;

        /// <summary>
        /// The main elements of hierarchies in the company.
        /// </summary>
        private readonly List<BaseForStaffer> _heads = new List<BaseForStaffer>();

        public List<BaseForStaffer> Heads
        {
            get
            {
                return _heads;
            }
        }

        public int _numberOfTreeLevels = 0; 

        private StafferLoadObject GetByIdAndRemoveFromList(int id, ref List<StafferLoadObject> staffers)
        {
            try
            {
                var resultStaffer = staffers.Single(s => s.Id == id);
                staffers.Remove(resultStaffer);
                return resultStaffer;
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine($"In {nameof(GetByIdAndRemoveFromList)} not only one element with Id:{id}");
                return new StafferLoadObject();
            }
        }

        public void BuildStaffersTrees()
        {
            var staffers = _stafferRepository.GetAll().ToList();

            // Elements from staffers are used to create elements derived from BaseForStaffer, from which a tree is built. The original elements in staffers are then deleted from source list.
            // The order of construction - first, the elements corresponding to head/root are searched for - then its subordinates are searched for and inserted for each element.

            int level = 1;
            var headsIndexes = GetHeadStaffersIndexes(staffers);

            foreach (int headId in headsIndexes)
            {
                var head = StafferFactory.CreateStaffer(GetByIdAndRemoveFromList(headId, ref staffers), level);                
                _heads.Add(head);
            }

            // Contains elements that are already included in the constructed tree.
            var insertedToTree = new List<BaseForStaffer>();

            // Contains elements from the underlying level (relative to the elements in insertedToTree) that will be added to the tree.
            var addingToTree = new List<BaseForStaffer>();

            insertedToTree.AddRange(_heads);
            ++level;
            while (insertedToTree.Count() > 0)
            {
                // By insertedToTree, form subordinate elements (to elements from insertedToTree) and deleting them in the staffers list.                
                // When forming, assign links to subordinates and enter links to elements in addingToTree.

                // TO DO a counter so that it does not hang when the structure is incorrect (by Count in staffers for example)                
                foreach (BaseForStaffer stafferInTree in insertedToTree)
                    if (stafferInTree.SubordinatedStaffers != null)
                    {
                        foreach (int subordinatedStaffersInd in stafferInTree.IndexesOfSubordinatedStaffers)
                        {
                            var subordinatedStaffer = StafferFactory.CreateStaffer(GetByIdAndRemoveFromList(subordinatedStaffersInd, ref staffers), level);                            
                            addingToTree.Add(subordinatedStaffer);
                            stafferInTree.SubordinatedStaffers.Add(subordinatedStaffer);
                        }
                    }

                // Move elements from addingToTree to insertedToTree.
                insertedToTree.Clear();
                insertedToTree.AddRange(addingToTree);
                addingToTree.Clear();
                ++level;
            }

            _numberOfTreeLevels = level - 2;    // Because when working, 2 additional ++levels are performed.
        }

        public void ShowTreeForHead(BaseForStaffer head)
        {
            //Console.WriteLine("".PadLeft(head.Level * 2, ' ') + head.Name);
            Console.WriteLine(head);
            foreach (BaseForStaffer staffer in head.SubordinatedStaffers)
                ShowTreeForHead(staffer);
        }
      
        private List<int> GetHeadStaffersIndexes(IEnumerable<StafferLoadObject> staffers)
        {
            // The top-level nodes set those staffers whose IDs are not in any of the IndexesOfSubordinated lists.

            var allIndexesContainedInSubordinated = new List<int>();

            foreach (StafferLoadObject slo in staffers)
                if (slo.IndexesOfSubordinated != null)
                    allIndexesContainedInSubordinated.AddRange(slo.IndexesOfSubordinated);

            allIndexesContainedInSubordinated = allIndexesContainedInSubordinated.Distinct().ToList();

            var result = new List<int>();
            foreach (StafferLoadObject slo in staffers)
                if (!allIndexesContainedInSubordinated.Contains(slo.Id))
                    result.Add(slo.Id);

            return result;
        }

        public void CalculateSalaryesForAllForDate(DateTime date)
        {
            // When calculating for a future date and a past date, the hierarchy of employees currently available is used.
            // But if the staffer did not work on the calculation date, then his salary is assumed to be zero and
            // the salaries of his subordinates are not taken into account in the calculation of the salaries of higher Salers.

            // The salary is calculated sequentially for all levels, starting from the lowest and ascending.
            foreach (BaseForStaffer head in _heads)
            {
                for (int level = _numberOfTreeLevels; level > 0; --level)
                    CalculateSalaryesForAllStaffersInLevelForDate(head, date, level);
            }
        }

        /// <summary>
        /// Calculation of salaries of staffers of the level "level" in the hierarchy for the date "date".
        /// Also, for these staffers, calculated summ of salaryes for the entire subtree (entered in the SalarySumForSubTree).
        /// </summary>
        /// <param name="date"></param>
        /// <param name="level"></param>
        private void CalculateSalaryesForAllStaffersInLevelForDate(BaseForStaffer head, DateTime date, int level)
        {
            var staffersOfLevel = GetAllStaffersWithLevel(head, level);
            foreach (BaseForStaffer staffer in staffersOfLevel)
                staffer.CalculateSalaryForDate(date);
        }
        
        public List<BaseForStaffer> GetAllStaffersWithLevel(BaseForStaffer head, int level)
        {
            var result = new List<BaseForStaffer>();
            TreeTraversal(head);
            return result;

            void TreeTraversal(BaseForStaffer head)
            {
                foreach (BaseForStaffer staffer in head.SubordinatedStaffers)
                    TreeTraversal(staffer);

                if (head.Level == level)
                    result.Add(head);
            }
        }

        public BaseForStaffer GetStafferById(BaseForStaffer head, int id)
        {
            BaseForStaffer result = null;
            FindNodeInTree(head);
            return result;

            void FindNodeInTree(BaseForStaffer head)
            {
                if (result != null) return;
                foreach (BaseForStaffer staffer in head.SubordinatedStaffers)
                    FindNodeInTree(staffer);

                if (head.Id == id)
                    result = head;
            }
        }

        public double GetTotalSalaryOfAllStaffersInSubTree(BaseForStaffer head)
        {
            return head.SalarySumForSubTree;
        }

        public double GetTotalSalaryOfAllCompanyStaffers()
        {
            double result = 0;
            foreach (BaseForStaffer head in _heads)
                result += GetTotalSalaryOfAllStaffersInSubTree(head);

            return result;
        }

        public CompanyStaffers(IStafferRepository stafferRepository)
        {
            _stafferRepository = stafferRepository;
        }
    }
}

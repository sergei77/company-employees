﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.Entities;
using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using System;
using System.Linq;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany
{    
    class Program
    {
        private static readonly string _nl = Environment.NewLine;

        static void Main(string[] args)
        {
            var companyStaffers = new CompanyStaffers(new TestCompanyRepository());

            companyStaffers.BuildStaffersTrees();

            var date = new DateTime(2021, 12, 24);
            Console.WriteLine($"Calculate salaries for: {date}");
            companyStaffers.CalculateSalaryesForAllForDate(date);

            Console.WriteLine($"Hierarchy and salaries for test company:{_nl}");
            foreach (BaseForStaffer head in companyStaffers.Heads)
            {
                Console.WriteLine($"Hierarchy for {head.Name}");
                companyStaffers.ShowTreeForHead(head);
            }


            var head1 = companyStaffers.Heads.ElementAt(0);
            var head2 = companyStaffers.Heads.ElementAt(1);

            var staffer31 = companyStaffers.GetStafferById(head2, 31);
            Console.WriteLine($"{_nl}staffer name : {staffer31.Name}  salary: {staffer31.Salary:.##}");

            var staffer6 = companyStaffers.GetStafferById(head1, 6);
            Console.WriteLine($"staffer name : {staffer6.Name}  salary: {staffer6.Salary:.##}");

            Console.WriteLine($"{_nl}Total Salary Of All Company : {companyStaffers.GetTotalSalaryOfAllCompanyStaffers()}");
 
            Console.WriteLine($"{_nl}Press Enter to complete.");
            Console.ReadLine();        
        }
    }
}

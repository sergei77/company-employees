﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using System;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Entities
{
    public class Employee : BaseForStaffer
    {
        public Employee(StafferLoadObject stafferLoadObject, int level) : base(stafferLoadObject, level)
        {
        }

        public override void CalculateSalaryForDate(DateTime date)
        {
            if (date >= _beginWorkAt)
            {
                _salary = _baseRate;

                // TO DO take into account leap years
                int numberOfWholeYearsWorked = (date - _beginWorkAt).Days / 365;
                double bonusForYears = 0.03 * _baseRate * numberOfWholeYearsWorked;
                if (bonusForYears > 0.3 * _baseRate)
                    bonusForYears = 0.3 * _baseRate;

                _salary += bonusForYears;

                _salarySumForSubTree = _salary;
            }
            else
            {
                _salary = 0;
                _salarySumForSubTree = 0;
            }
        }

        protected override char CharForTypeOfStaffer()
        {
            return 'e';
        }
    }
}

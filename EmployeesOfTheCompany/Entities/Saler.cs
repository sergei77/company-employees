﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using System;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Entities
{
    public class Saler : BaseForStaffer
    {
        public Saler(StafferLoadObject stafferLoadObject, int level) : base(stafferLoadObject, level)
        {
        }

        public override void CalculateSalaryForDate(DateTime date)
        {
            if (date >= _beginWorkAt)
            {
                _salary = _baseRate;

                // TO DO take into account leap years
                int numberOfWholeYearsWorked = (date - _beginWorkAt).Days / 365;
                double bonusForYears = 0.01 * _baseRate * numberOfWholeYearsWorked;
                if (bonusForYears > 0.35 * _baseRate)
                    bonusForYears = 0.35 * _baseRate;

                _salary += bonusForYears;

                // Find a bonus from the salaries of all subordinates and calculate the amount of salaries for a subtree with this element as the root.
                double salarySumForSubTree = 0;
                foreach (BaseForStaffer subordinated in SubordinatedStaffers)
                {
                    _salary += 0.003 * subordinated.SalarySumForSubTree;
                    salarySumForSubTree += subordinated.SalarySumForSubTree;
                }

                _salarySumForSubTree = _salary + salarySumForSubTree;
            }
            else
            {
                _salary = 0;
                _salarySumForSubTree = 0;   // Because he was not working at that moment, we do not take into salaries of subordinates.
            }
        }

        protected override char CharForTypeOfStaffer()
        {
            return 's';
        }
    }
}

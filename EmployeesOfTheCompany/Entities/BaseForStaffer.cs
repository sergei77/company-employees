﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using System;
using System.Collections.Generic;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Entities
{
    public abstract class BaseForStaffer
    {
        protected static readonly uint _baseRate = 2000;

        protected readonly int _id;
        protected readonly string _name;
        protected readonly DateTime _beginWorkAt;
        protected double _salary = 0;
        protected double _salarySumForSubTree = 0;
        protected int _level;

        public int Id => _id;
        public string Name => _name;
        public DateTime BeginWorkAt => _beginWorkAt;

        public int Level => _level;

        public uint BaseRate => _baseRate;

        public double Salary => _salary;

        // The sum of the salaries of all elements of the subtree, starting from this node (including this node).
        public double SalarySumForSubTree => _salarySumForSubTree;

        public BaseForStaffer(StafferLoadObject stafferLoadObject, int level) :
            this(stafferLoadObject.Id, stafferLoadObject.IndexesOfSubordinated, stafferLoadObject.Name, stafferLoadObject.BeginWorkAt, level)
        {
        }

        public BaseForStaffer(int Id, List<int> indexesOfSubordinatedStaffers, string name, DateTime beginWorkAt, int level)
        {
            if (Id < 0)
                throw new ArgumentException($"передан Id < 0 в конструктор {nameof(BaseForStaffer)}");

            _id = Id;
            _name = name;
            _beginWorkAt = beginWorkAt;
            _level = level;
            if (indexesOfSubordinatedStaffers != null)
                IndexesOfSubordinatedStaffers.AddRange(indexesOfSubordinatedStaffers);
        }

        public List<int> IndexesOfSubordinatedStaffers = new List<int>();
        public List<BaseForStaffer> SubordinatedStaffers = new List<BaseForStaffer>();

        public abstract void CalculateSalaryForDate(DateTime date);

        protected abstract char CharForTypeOfStaffer();

        public override string ToString()
        {
            return $"{"".PadLeft(_level * 2, ' ')} {_name} :{CharForTypeOfStaffer()}  - {_salary:.##}";
        }
    }
}

﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using NUnit.Framework;
using System.Linq;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Tests
{
    [TestFixture]
    class BuildingTreeTest
    {
        [Test]
        public void BuildingTreeForTestCompany_UseTestCompanyRepository_Pass()
        {
            var companyStaffers = new CompanyStaffers(new TestCompanyRepository());

            var heads = companyStaffers.Heads;
            Assert.AreEqual(heads.Count(), 0);

            companyStaffers.BuildStaffersTrees();
            var A = heads.Where(s => s.Name == "A").Single();

            // Check for Levels
            var staffersWithLevel2 = companyStaffers.GetAllStaffersWithLevel(A, 2);
            Assert.AreEqual(staffersWithLevel2.Count(), 3);
            Assert.AreEqual(staffersWithLevel2.Where(s => s.Name == "B").Count(), 1);
            Assert.AreEqual(staffersWithLevel2.Where(s => s.Name == "C").Count(), 1);
            Assert.AreEqual(staffersWithLevel2.Where(s => s.Name == "D").Count(), 1);

            var staffersWithLevel3 = companyStaffers.GetAllStaffersWithLevel(A, 3);
            Assert.AreEqual(staffersWithLevel3.Count(), 4);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "E").Count(), 1);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "F").Count(), 1);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "G").Count(), 1);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "H").Count(), 1);
        }
    }
}

﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.Entities;
using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using NUnit.Framework;
using System.Linq;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Tests
{
    [TestFixture]
    public class StafferRepositoryTests
    {
        private TestCompanyRepository _stafferLoader;

        [SetUp]
        public void Setup()
        {
            _stafferLoader = new TestCompanyRepository();
        }

        [Test]
        public void TestCompanyStaffersLoad_AllLoad_PassTest()
        {            
            var staffers = _stafferLoader.GetAll();
            var staffersWithId12 = staffers.Where(slo => slo.Id == 12);

            Assert.AreEqual(staffers.Count(), 16);
            Assert.AreEqual(staffersWithId12.Count(), 1);
            Assert.AreEqual(staffersWithId12.ElementAt(0).Name, "G");
            //Assert.Pass();
        }

        [Test]
        public void BuildingTreeForTestCompany_UseTestCompanyRepository_Pass()
        {
            var companyStaffers = new CompanyStaffers(_stafferLoader);

            var heads = companyStaffers.Heads;
            Assert.AreEqual(heads.Count(), 0);

            companyStaffers.BuildStaffersTrees();

            // Check for Heads
            Assert.AreEqual(heads.Count(), 2);
            var headA = heads.Where(s => s.Name == "A");
            var headP = heads.Where(s => s.Name == "P");

            Assert.IsNotNull(headA);
            Assert.IsNotNull(headP);
            Assert.AreEqual(headA.Count(), 1);
            Assert.AreEqual(headP.Count(), 1);

            var A = headA.ElementAt(0);
            var P = headP.First();

            Assert.AreEqual(A.Id, 3);
            Assert.AreEqual(P.Id, 30);

            Assert.IsTrue(A.GetType() == typeof(Manager));
            Assert.IsTrue(P.GetType() == typeof(Manager));

            // Check for Levels
            var staffersWithLevel2 = companyStaffers.GetAllStaffersWithLevel(A, 2);
            Assert.AreEqual(staffersWithLevel2.Count(), 3);
            Assert.AreEqual(staffersWithLevel2.Where(s => s.Name == "B").Count(), 1);
            Assert.AreEqual(staffersWithLevel2.Where(s => s.Name == "C").Count(), 1);
            Assert.AreEqual(staffersWithLevel2.Where(s => s.Name == "D").Count(), 1);

            var staffersWithLevel3 = companyStaffers.GetAllStaffersWithLevel(A, 3);
            Assert.AreEqual(staffersWithLevel3.Count(), 4);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "E").Count(), 1);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "F").Count(), 1);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "G").Count(), 1);
            Assert.AreEqual(staffersWithLevel3.Where(s => s.Name == "H").Count(), 1);
        }
    }
}

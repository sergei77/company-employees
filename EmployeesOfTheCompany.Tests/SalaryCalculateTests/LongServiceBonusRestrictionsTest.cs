﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.Entities;
using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using NUnit.Framework;
using System.Linq;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Tests
{

    [TestFixture]
    class LongServiceBonusRestrictionsTest
    {
        private const uint _bR = 2000;
        private TestCompanyRepository _stafferLoader;

        [SetUp]
        public void Setup()
        {
            _stafferLoader = new TestCompanyRepository();
        }

        [Test]

        // For employee
        [TestCase(5, 0, _bR)]
        [TestCase(5, -10, 0)]
        [TestCase(5, 366, 1.03*_bR)]
        [TestCase(5, 10*366, 1.3 * _bR)]
        [TestCase(5, 11*366, 1.3 * _bR)]

        // For manager
        [TestCase(7, 0, _bR)]
        [TestCase(7, -10, 0)]
        [TestCase(7, 366, 1.05 * _bR)]
        [TestCase(7, 8*366, 1.4 * _bR)]
        [TestCase(7, 9*366, 1.4 * _bR)]

        // For saler
        [TestCase(6, 0, _bR)]
        [TestCase(6, -10, 0)]
        [TestCase(6, 366, 1.01 * _bR)]
        [TestCase(6, 35 * 366, 1.35 * _bR)]
        [TestCase(6, 36 * 366, 1.35 * _bR)]
        public void CalculateSalaryForDate_CalculateWithDifferentExperiences_CalculatedByRules(int id, int daysOfExperience, double salary)
        {
            var staffers = _stafferLoader.GetAll();
            var stafferLoadObjectWithId = staffers.Where(slo => slo.Id == id).Single();
            BaseForStaffer testedStaffer = StafferFactory.CreateStaffer(stafferLoadObjectWithId, 1);

            var bBeginWorkAt = testedStaffer.BeginWorkAt;

            testedStaffer.CalculateSalaryForDate(bBeginWorkAt.AddDays(daysOfExperience));
            Assert.AreEqual(testedStaffer.Salary, salary);
            Assert.AreEqual(testedStaffer.SalarySumForSubTree, salary);
        }
    }
}

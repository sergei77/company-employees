﻿using Aspose.TestJobSergei.EmployeesOfTheCompany;
using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using NUnit.Framework;
using System;
using System.Linq;

namespace EmployeesOfTheCompany.Tests.SalaryCalculateTests
{
    [TestFixture]
    class SalaryCalculateTestFor_24_12_2022
    {
        private const uint _bR = 2000;

        private CompanyStaffers _testCompanyStaffers;

        [SetUp]
        public void Setup()
        {
            _testCompanyStaffers = new CompanyStaffers(new TestCompanyRepository());
            _testCompanyStaffers.BuildStaffersTrees();
            _testCompanyStaffers.CalculateSalaryesForAllForDate(new DateTime(2022, 12, 24));
        }

        [Test]
        [TestCase(14, 1.05 * _bR)]
        [TestCase(15, 1.03 * _bR)]
        [TestCase(18, 1.03 * _bR)]
        [TestCase(24, 1.06 * _bR)]
        [TestCase(25, 1.05 * _bR)]
        [TestCase(9, 1.03 * _bR)]
        [TestCase(10, 1.01 * _bR + 0.003 * 3.11 * _bR)]
        [TestCase(12, 1.2 * _bR + 0.005 * 2.11 * _bR)]        
        [TestCase(6, 1.08 * _bR + 0.003 * (
            6.25 * _bR +
            1.01*_bR + 0.003 * 3.11 * _bR +
            1.2 * _bR + 0.005 * 2.11 * _bR))]

        [TestCase(21, 1.15 * _bR)]

        public void CalculateSalaryesForAllForDate_ForTestCompanyHead1_EqualsWithManyallyCalculated(int id, double salary)
        {
            var _head1 = _testCompanyStaffers.Heads.ElementAt(0);

            var staffer = _testCompanyStaffers.GetStafferById(_head1, id);
            Assert.IsTrue(Math.Abs(staffer.Salary - salary) < 0.00001);
        }

        [Test]
        [TestCase(31, 1.04 * _bR)]
        [TestCase(33, 1.04 * _bR)]
        [TestCase(30, 1.2 * _bR + 0.005 * 2.08 * _bR)]
        public void CalculateSalaryesForAllForDate_ForTestCompanyHead2_EqualsWithManyallyCalculated(int id, double salary)
        {
            var _head2 = _testCompanyStaffers.Heads.ElementAt(1);

            var staffer = _testCompanyStaffers.GetStafferById(_head2, id);
            Assert.IsTrue(Math.Abs(staffer.Salary - salary) < 0.000001);
        }

    }
}

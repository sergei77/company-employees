﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using NUnit.Framework;
using System;
using System.Linq;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Tests
{
    [TestFixture]
    class SalaryCalculateTestFor_01_02_2018
    {
        private const uint _bR = 2000;

        private CompanyStaffers _testCompanyStaffers;

        [SetUp]
        public void Setup()
        {
            _testCompanyStaffers = new CompanyStaffers(new TestCompanyRepository());
            _testCompanyStaffers.BuildStaffersTrees();
            _testCompanyStaffers.CalculateSalaryesForAllForDate(new DateTime(2018, 2, 1));
        }

        [Test]
        [TestCase(14, _bR)]
        [TestCase(15, 0)]
        [TestCase(18, 0)]
        [TestCase(24, 1.01 * _bR)]
        [TestCase(25, 0)]
        [TestCase(10, 0)]
        [TestCase(12, 0)]
        [TestCase(9, 0)]
        [TestCase(6, 1.03 * _bR)]        

        public void CalculateSalaryesForAllForDate_ForTestCompanyHead1_EqualsWithManyallyCalculated(int id, double salary)
        {
            var _head1 = _testCompanyStaffers.Heads.ElementAt(0);

            var staffer = _testCompanyStaffers.GetStafferById(_head1, id);
            Assert.IsTrue(Math.Abs(staffer.Salary - salary) < 0.00001);
        }

        [Test]
        [TestCase(31, _bR)]
        [TestCase(33, _bR)]
        [TestCase(30, _bR + 0.005*2*_bR)]
        public void CalculateSalaryesForAllForDate_ForTestCompanyHead2_EqualsWithManyallyCalculated(int id, double salary)
        {
            var _head2 = _testCompanyStaffers.Heads.ElementAt(1);

            var staffer = _testCompanyStaffers.GetStafferById(_head2, id);
            Assert.IsTrue(Math.Abs(staffer.Salary - salary) < 0.000001);
        }

    }
}

﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using NUnit.Framework;
using System;
using System.Linq;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Tests
{
    [TestFixture]
    class SalaryCalculateTestFor_24_12_2021
    {
        private const uint _bR = 2000;

        private CompanyStaffers _testCompanyStaffers;

        [SetUp]
        public void Setup()
        {
            _testCompanyStaffers = new CompanyStaffers(new TestCompanyRepository());
            _testCompanyStaffers.BuildStaffersTrees();
            _testCompanyStaffers.CalculateSalaryesForAllForDate(new DateTime(2021, 12, 24));
        }

        [Test]
        [TestCase(14, 1.04 * _bR)]
        [TestCase(15, 1.02 * _bR)]
        [TestCase(18, _bR)]
        [TestCase(24, 1.05 * _bR)]
        [TestCase(25, _bR)]
        [TestCase(10, _bR + 0.003 * 3.06 * _bR)]
        [TestCase(12, 1.15 * _bR + 0.005 * 2.05 * _bR)]
        [TestCase(9, 1.02 * _bR)]
        [TestCase(6, 1.07 * _bR + 0.003 * (
            5.11 * _bR +
            _bR + 0.003 * 3.06 * _bR +
            1.15 * _bR + 0.005 * 2.05 * _bR +
            1.02 * _bR))]

        [TestCase(21, 1.12 * _bR)]

        public void CalculateSalaryesForAllForDate_ForTestCompanyHead1_EqualsWithManyallyCalculated(int id, double salary)
        {
            var _head1 = _testCompanyStaffers.Heads.ElementAt(0);

            var staffer = _testCompanyStaffers.GetStafferById(_head1, id);
            Assert.IsTrue(Math.Abs(staffer.Salary - salary) < 0.00001);
        }

        [Test]
        [TestCase(31, 1.03 * _bR)]
        [TestCase(33, 1.03 * _bR)]
        [TestCase(30, 1.15 * _bR + 0.005 * 2.06 * _bR)]
        public void CalculateSalaryesForAllForDate_ForTestCompanyHead2_EqualsWithManyallyCalculated(int id, double salary)
        {
            var _head2 = _testCompanyStaffers.Heads.ElementAt(1);

            var staffer = _testCompanyStaffers.GetStafferById(_head2, id);
            Assert.IsTrue(Math.Abs(staffer.Salary - salary) < 0.000001);
        }

        [Test]
        public void GetTotalSalaryOfAllStaffersInSubTree_ForTestCompanyForStafferWithId6_EqualsWithManyallyCalculated()
        {
            var _head1 = _testCompanyStaffers.Heads.ElementAt(0);
            var C = _testCompanyStaffers.GetStafferById(_head1, 6);
            var totalSalary = _testCompanyStaffers.GetTotalSalaryOfAllStaffersInSubTree(C);

            // Сalculate amount of salaries of staffers of the subtree starting with C
            var I = _testCompanyStaffers.GetStafferById(_head1, 14);
            var J = _testCompanyStaffers.GetStafferById(_head1, 15);
            var K = _testCompanyStaffers.GetStafferById(_head1, 18);
            var L = _testCompanyStaffers.GetStafferById(_head1, 24);
            var M = _testCompanyStaffers.GetStafferById(_head1, 25);
            var E = _testCompanyStaffers.GetStafferById(_head1, 9);
            var F = _testCompanyStaffers.GetStafferById(_head1, 10);
            var G = _testCompanyStaffers.GetStafferById(_head1, 12);

            double totalSalaryForCheck = I.Salary + J.Salary + K.Salary + L.Salary + M.Salary + + E.Salary + F.Salary + G.Salary + C.Salary;

            Assert.IsTrue(Math.Abs(totalSalary - totalSalaryForCheck) < 0.00001);
        }
    }
}

﻿using Aspose.TestJobSergei.EmployeesOfTheCompany.Entities;
using Aspose.TestJobSergei.EmployeesOfTheCompany.StafferRepository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspose.TestJobSergei.EmployeesOfTheCompany.Tests
{
    [TestFixture]
    class GetStafferByIdTest
    {
        private CompanyStaffers _testCompanyStaffers;
        private BaseForStaffer _head1;
        private BaseForStaffer _head2;

        [SetUp]
        public void Setup()
        {            
            _testCompanyStaffers = new CompanyStaffers(new TestCompanyRepository());
            _testCompanyStaffers.BuildStaffersTrees();
            _head1 = _testCompanyStaffers.Heads.ElementAt(0);
            _head2 = _testCompanyStaffers.Heads.ElementAt(1);
        }

        [Test]
        [TestCase(7, "D")]
        [TestCase(18, "K")]
        [TestCase(21, "H")]
        [TestCase(3, "A")]
        [TestCase(25, "M")]
        public void GetStafferById_TestCompanyHead1_NamesOfFoundedEqualsToGiven(int id, string name)
        {
            var staffer = _testCompanyStaffers.GetStafferById(_head1, id);

            Assert.AreEqual(staffer.Name, name);
        }

        [Test]
        [TestCase(30, "P")]
        [TestCase(31, "Q")]
        [TestCase(33, "R")]
        public void GetStafferById_TestCompanyHead2_NamesOfFoundedEqualsToGiven(int id, string name)
        {
            var staffer = _testCompanyStaffers.GetStafferById(_head2, id);

            Assert.AreEqual(staffer.Name, name);
        }

        [Test]
        [TestCase(1)]
        [TestCase(20)]
        [TestCase(39)]
        public void GetStafferById_TestCompanyNonExistingIds_ReturnsNull(int id)
        {
            var staffer = _testCompanyStaffers.GetStafferById(_head1, id);
            Assert.IsNull(staffer);

            staffer = _testCompanyStaffers.GetStafferById(_head2, id);
            Assert.IsNull(staffer);
        }
    }
}
